// ----------------------------------------------------------------------------------------------------------------- Permet d'activer le Readline
const readline = require('./projet_nodejs/node_modules/readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});



// ----------------------------------------------------------------------------------------------------------------- Variable pour les tableaux.
let contact = []; // ----------------------------------------------------------------------------------------------- Créer un tableau vide pour stocker les contacts que l'on créer
let listContact = []; // ------------------------------------------------------------------------------------------- Créer un tableau vide ou l'on va push les noms et prénom
let phone = []; // ------------------------------------------------------------------------------------------------- Créer un tableau vide ou l'on va push les numéros de téléphone
let regexPhone = /^(0)[6](\d{2}){4}$/; // -------------------------------------------------------------------------- Regex pour les numéros de téléphones
let regexName = /^[A-Z]([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$/; // ------------------------------------------------------- Regex pour les noms et prénoms 



// ----------------------------------------------------------------------------------------------------------------- Question poser au départ.
rl.question('Bonjour, selectionner /help, si vous désirez obtenir de l\'aide. \nSinon, entré une commande pour commencer. \n\n', (answer) => {

    if (answer === "/add") { // ------------------------------------------------------------------------------------ Si on tappe /add, alors on lance la function Add();
        Add();
    } else if (answer === "/help") { // ---------------------------------------------------------------------------- Si on tappe /help, alors on lance la function Help();
        Help();
    } else if (answer === "/stop") { // ---------------------------------------------------------------------------- Si on tappe /stop, alors on lance la function Stop();
        Stop();
    } else if (answer === "/list") { // ---------------------------------------------------------------------------- Si on tappe /list, alors on lance la function List();
        List();
    } else if (answer === "/delete") { // -------------------------------------------------------------------------- Si on tappe /delete, alors on lance la function Delete();
        Delete();
    } else { // ---------------------------------------------------------------------------------------------------- Si l'on tappe quelque chose qui ne correspond pas, on à un message d'erreur
        console.log(`La commande suivante n'est pas enregistré : ${answer}`);
        question(); // --------------------------------------------------------------------------------------------- Permet de continuer à utiliser les /add, /help etc...
    }
});



// ----------------------------------------------------------------------------------------------------------------- Function pour mon /help
Help = () => {
    console.log("/add : Permet d'ajouter un contact.\n");
    console.log("/stop : Permet de fermer complètement la ReadLine.\n");
    console.log("/list : Affiche la liste des contacts.\n");
    console.log("/delete : Permet d'effacer un contact.\n");
    question(); // ------------------------------------------------------------------------------------------------- Permet de continuer à utiliser les /add, /help etc...
};



// ----------------------------------------------------------------------------------------------------------------- Function pour ajouter des nouveaux contacts
Add = () => {
    rl.question(`Quel est le prenom de votre contact ? \n`, (prenom) => { //---------------------------------------- Question qui va s'afficher pour rentrer le prénom du contact

        if (regexName.test(prenom)) { // --------------------------------------------------------------------------- Permet de tester le prénom avec le regexName.test(prenom)
            contact.push(prenom); // ------------------------------------------------------------------------------- Push le prénon choisie dans le tableau contact
            console.log(`Prénom : ${prenom}\n`); // ---------------------------------------------------------------- Affiche le prénom choisie

        } else { // ------------------------------------------------------------------------------------------------ Si le regex ne valide pas le prénom, alors il affiche une erreur
            console.log("Perdu ! Recommence ;)\n")
            Add(); // ---------------------------------------------------------------------------------------------- Relance la function Add en cas d'erreur 
            contact = []; // --------------------------------------------------------------------------------------- Fait en sorte que le tableau soit clear
        }


        rl.question(`Quel est le nom de famille de votre contact ?  :  \n`, (famille) => { //----------------------- Question qui va s'afficher pour rentrer le nom de famille du contact

            if (regexName.test(famille)) { // ---------------------------------------------------------------------- Permet de tester le nom de famille avec le regexName.test(famille)
                contact.push(famille); // -------------------------------------------------------------------------- Push le nom de famille choisie dans le tableau contact
                console.log(`Nom de famille : ${famille}\n`); // --------------------------------------------------- Affiche le nom de famille choisi

            } else { // -------------------------------------------------------------------------------------------- Si le regex ne valide pas le nom de famille, alors il affiche une erreur
                console.log("Perdu ! Recommence ;)\n")
                Add(); // ------------------------------------------------------------------------------------------ Relance la function Add en cas d'erreur 
                contact = []; // ----------------------------------------------------------------------------------- Fait en sorte que le tableau soit clear
            }


            rl.question(`Quel est le numero de ${prenom} ${famille} ? \n`, (numTel) => { //------------------------- Question qui va s'afficher pour rentrer le numero du contact

                if (regexPhone.test(numTel)) { // ------------------------------------------------------------------ Permet de tester le numéro de téléphone avec le regexPhone.test(numTel)
                    phone.push(numTel); // ------------------------------------------------------------------------- Push le numero de téléphone choisi dans le tableau phone
                    console.log(`Numéro ${numTel}\n`); // ---------------------------------------------------------- Affiche le numéro choisi
                    console.log(`Votre nouveau contact est : \n- ${prenom} \n- ${famille} \n- n°: ${numTel} \n`); // Permet de faire un résumé du contact créer

                    contact = contact.join(" "); // ---------------------------------------------------------------- Permet transformer le prénom et le nom en un seul élément
                    listContact.push(contact); // ------------------------------------------------------------------ Push le prénom et le nom assembler au-dessus dans le tableau listContact
                    contact = []; // ------------------------------------------------------------------------------- Fait en sorte que le tableau soit clear
                    question(); // --------------------------------------------------------------------------------- Permet de continuer à utiliser les /add, /help etc...

                } else { // ---------------------------------------------------------------------------------------- Si le regex ne valide pas le nom de famille, alors il affiche une erreur
                    console.log("Perdu ! Recommence ;)\n")
                    Add(); // -------------------------------------------------------------------------------------- Relance la function Add en cas d'erreur 
                    contact = []; // ------------------------------------------------------------------------------- Fait en sorte que le tableau soit clear
                }


            });
        });
    })
};



// ----------------------------------------------------------------------------------------------------------------- Function pour afficher la List des contacts
List = () => {
    console.log("Mes contacts : \n----- ------ ------ \n");
    for (i = 0; i < listContact.length; i++) { //------------------------------------------------------------------- i (index) est = à 0; Si i inférieur à listeContact. length; alors i++
        listContact[i];
        phone[i];
        console.log(`Contact : ${[i+1]} ==> ${listContact[i]}\nTelephone : ${phone[i]}\n`); // --------------------- Permet d'aller chercher les différents éléments des tableaux pour afficher les contacts
    }
    question(); // ------------------------------------------------------------------------------------------------- Permet de continuer à utiliser les /add, /help etc...
};



// ----------------------------------------------------------------------------------------------------------------- Function pour supprimer un contact
Delete = () => {
    rl.question("Pour supprimer un contact, merci de choisir le numéro du contact associer.\n", (Suppr) => { // ---- Question qui va s'afficher pour selectionner le contact à delete

        listContact.splice([Suppr - 1], 1); // --------------------------------------------------------------------- Supprime le contact choisie dans listContact
        phone.splice([Suppr - 1], 1); // --------------------------------------------------------------------------- Supprime le contact choisie dans phone

        console.log(`Bravo, vous avez supprimé le contact numéro ${Suppr}`) // ------------------------------------- Affiche le l'id du contact supprimé 
        question(); // --------------------------------------------------------------------------------------------- Permet de continuer à utiliser les /add, /help etc...
    })
};



// ----------------------------------------------------------------------------------------------- Function pour stopper.
Stop = () => {
    rl.close(); // ------------------------------------------------------------------------------- Met fin a l'appli
};



// ----------------------------------------------------------------------------------------------- Function pour continuer d'utilisé mes /add, /help etc...
question = () => {
    rl.question("", (answer) => { // ------------------------------------------------------------- Ici la question est vide car la function sert juste à pouvoir entrer d'autre commande

        if (answer === "/add") { // -------------------------------------------------------------- Si on tappe /add, alors on lance la function Add();
            Add();
        } else if (answer === "/help") { // ------------------------------------------------------ Si on tappe /help, alors on lance la function Help();
            Help();
        } else if (answer === "/stop") { // ------------------------------------------------------ Si on tappe /stop, alors on lance la function Stop();
            Stop();
        } else if (answer === "/list") { // ------------------------------------------------------ Si on tappe /list, alors on lance la function List();
            List();
        } else if (answer === "/delete") { // ---------------------------------------------------- Si on tappe /delete, alors on lance la function Delete();
            Delete();
        } else { // ------------------------------------------------------------------------------ Si l'on tappe quelque chose qui ne correspond pas, on à un message d'erreur
            console.log(`La commande suivante n'est pas enregistré : ${answer}`);
            question(); // ----------------------------------------------------------------------- Permet de continuer à utiliser les /add, /help etc...
        }
    });
};